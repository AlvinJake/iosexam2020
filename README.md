## Swift Base Project

iOS Exam 2020

## Base Tooling

- RxSwift for data flow management.

- Rxcocoa a framework that makes Cocoa APIs used in iOS easier to use with reactive techniques.

- Alamofire for networking.

- Kingfisher a lightweight, pure-Swift library for downloading and caching images from the web.

- Dip for dependency injection

- Result represents either a success or a failure.





## Architecture



- I am going to use “CLEAN SWIFT ARCHITECTURE”.



- Models

The Models class will be related to each component, It will be of type struct and mostly it will contain Request, Response, and ViewModel structs.



- Router

The router takes care for the transition and passing data between view controllers.



- Worker

The Worker component will handle all the API/CoreData requests and responses.



- Interactor

This is the “mediator” between the Worker and the Presenter. It communicates with the ViewController which passes all the Request params needed for the Worker. The Worker returns a response and the Interactor passes that response towards the Presenter.



- Presenter

 Presenter will be in charge of the presentation logic. 



![alt text](https://AlvinJake@bitbucket.org/AlvinJake/iosexam2020.git/iOSExam/Clean Swift.png)



## Issue



- Issue on displaying images



[/BuildRoot/Library/Caches/com.apple.xbs/Sources/CoreUI_Sim/CoreUI-499.12/Bom/Storage/BOMStorage.c:516] <memory> is not a BOMStorage file
