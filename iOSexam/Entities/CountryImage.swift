//
//  CountryImage.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryImageProtocol {
    var stringURL: String { get set }
    func url() -> URL
}

// MARK: Image Protocol

extension CountryImageProtocol {
    public func url() -> URL {
        return URL(string: self.stringURL)!
    }
}

public struct CountryImage: CountryImageProtocol {
    public var stringURL: String
    
    public init(stringUrl strURL: String) {
        self.stringURL = strURL
    }
}
