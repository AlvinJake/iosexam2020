//
//  Country.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryProtocol {
    var name: String { get set }
    var cioc: String { get set }
    var population: Double { get set }
    var images: Array<CountryImageProtocol>? { get set }
    var capital: String { get set }
    var alphaCode: String { get set }
}

public struct Country: CountryProtocol {
    public var name: String
    public var cioc: String
    public var population: Double
    public var images: Array<CountryImageProtocol>?
    public var capital: String
    public var alphaCode: String
    
    public init(name: String, cioc: String, population: Double, capital: String, alphaCode: String) {
        self.name = name
        self.cioc = cioc
        self.population = population
        self.capital = capital
        self.alphaCode = alphaCode
    }
}
