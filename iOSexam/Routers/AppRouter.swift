//
//  AppRouter.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit

class AppRouter: NSObject, AppRoutingLogic {
    
    public var controller: AppControllerProtocol!
    var appRouter: AppRoutingLogic!
    var window: UIWindow!
    var storyboard: UIStoryboard!
    
    public func moveToCountry() {
        let viewController = storyboard.instantiateViewController(withIdentifier: "CountryViewController")
        let navController = UINavigationController(rootViewController: viewController)
        window!.rootViewController = navController
        window!.makeKeyAndVisible()
    }
    
}

