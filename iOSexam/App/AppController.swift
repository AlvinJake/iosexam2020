//
//  AppController.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit

class AppController: NSObject, AppControllerProtocol {
    func restAPI() {
        
    }
    
    var router: AppRoutingLogic!
    var interactor: AppBusinessLogic!
    var presenter: AppPresentationLogic!
    
    func didLaunch() {
        displayUI()
        startServices()
    }
    
    private func displayUI() {
        router.moveToCountry()
    }
    
    private func startServices() {
        interactor.initializeServices()
    }
    
    func restAPI() -> APIProtocol {
        return interactor.restAPI
    }
    
    func dataStore() -> DataStoreProtocol {
        return self.interactor.dataStore()
    }
}
