//
//  FakeRemoteApi.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 10/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Result

class FakeRemoteAPI: NSObject, APIProtocol {
    
    var countryResults: [Country] = []
    
    func initialize() {}
    
    
    func fetchRemoteCountry(completion: @escaping CountryRequestCompletionBlock) {
        completion(Result.success(self.countryResults))
    }
}
