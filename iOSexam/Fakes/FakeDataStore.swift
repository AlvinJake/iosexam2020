//
//  FakeDataStore.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 10/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Result

class FakeDataStore: NSObject, DataStoreProtocol {
    
    
    func fetchCountry(completion: @escaping CountryFetchCompletionBlock) {
        let image = CountryImage(stringUrl: "https://restcountries.eu/data/bmu.svg")
        var country = Country(name: "IceLand", cioc: "IL", population : 10.000, capital: "REYKJAVIK", alphaCode: "ICE")
        country.images = [image]
        
        
        var country2 = Country(name: "India", cioc: "IND", population : 30.000, capital: "NEW DELHI", alphaCode: "IND")
        let image2 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country2.images = [image2]
        
        var country3 = Country(name: "South Korea", cioc: "SK", population : 50.000, capital: "SEOUL", alphaCode: "SEOUL")
        let image3 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country3.images = [image3]
        
        var country4 = Country(name: "Japan", cioc: "JPN", population : 20.000, capital: "TOKYO", alphaCode: "TKY")
        let image4 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country4.images = [image4]
        
        var country5 = Country(name: "PHilippines", cioc: "PH", population : 100.000, capital: "MANILA", alphaCode: "MNL")
        let image5 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country5.images = [image5]
        
        var country6 = Country(name: "Australia", cioc: "AU", population : 60.000, capital: "SYDNEY", alphaCode: "SDNY")
        let image6 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country6.images = [image6]
        
        var country7 = Country(name: "Algeria", cioc: "ALG", population : 80.000, capital: "ALGIERS", alphaCode: "ALGY")
        let image7 = CountryImage(stringUrl: "https://restcountries.eu/data/blz.svg")
        country7.images = [image7]
        
        
        let countries: [CountryProtocol] = [country, country2, country3, country4, country5, country6, country7]
        completion(Result.success(countries))
    }
    
    
}
