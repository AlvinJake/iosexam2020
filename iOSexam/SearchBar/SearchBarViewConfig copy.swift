

import UIKit

class SearchBarViewConfig: NSObject {
    var searchController: UISearchController
    var viewController: UIViewController
    var searchControllerDelegate: UISearchControllerDelegate!
    var searchBarDelegate: UISearchBarDelegate!
    var searchResultsUpdatingDelegate: UISearchResultsUpdating!
    
    
    init(hostViewController viewController:UIViewController, forController controller:UISearchController) {
        self.searchController = controller
        self.viewController = viewController
    }
    
    func configure() {
        self.searchController.searchResultsUpdater = self.searchResultsUpdatingDelegate
        self.searchController.delegate = self.searchControllerDelegate
        self.searchController.searchBar.delegate = self.searchBarDelegate
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search country"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.becomeFirstResponder()
        self.viewController.navigationItem.titleView = searchController.searchBar
    }
}
