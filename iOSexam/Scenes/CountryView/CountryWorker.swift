//
//  CountryWorker.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryWorkerProtocol {
    var countryAppInterface: AppCountryManagementProtocol? { get set }
    func fetchCountry(completion: @escaping CountryFetchCompletionBlock)
}

class CountryWorker: CountryWorkerProtocol {
    
    public var countryAppInterface: AppCountryManagementProtocol?
    
    //fetch from local datastore
    public func fetchCountry(completion: @escaping CountryFetchCompletionBlock) {
        countryAppInterface?.fetchCountry(completion: completion)
    }
    
    public init() {
        
    }
    
}
