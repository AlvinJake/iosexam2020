//
//  CountryPresenter.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryViewDisplayLogic: class {
    func reloadCountry(country: [CountryViewModel])
}

public protocol CountryViewPresentationLogic {
    func presentCountry(country: [CountryProtocol])
    func presentError(errorDesc: String)
}

public class CountryPresenter: CountryViewPresentationLogic {
    public weak var viewController: (CountryViewDisplayLogic)?
    
    public init(){}
    
    public func presentCountry(country: [CountryProtocol]) {
        
        
        DispatchQueue.main.async {
            var countryViewModels = [CountryViewModel]()
            
            countryViewModels = country.map({ (country) -> CountryViewModel in
                var prod = CountryViewModel(name: country.name, cioc: country.cioc, population: country.population.format(f: ".1"), capital: country.capital, alphaCode: country.alphaCode )
                prod.image = country.images!.first!.url()
                return prod
            })
            self.viewController?.reloadCountry(country: countryViewModels)
        }
        
    }
    
    public func presentError(errorDesc: String) {
        print("Error: \(errorDesc)")
    }
}
