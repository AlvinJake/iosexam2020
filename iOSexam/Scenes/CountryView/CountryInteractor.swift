//
//  CountryInteractor.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryViewRoutingLogic
{
    func routeToCountryDetail()
}

public protocol CountryViewBusinessLogic
{
    var appController: AppControllerProtocol! { get set }
    func displayCountry()
    func selected(countryAtIndexPath: IndexPath)
}

class CountryInteractor: CountryViewBusinessLogic {
    var appController: AppControllerProtocol!
    
    var country = [CountryProtocol]()
    
    public var dataTransporter: AnyDataTransport<CountryProtocol, CountryViewModelProtocol>?
    
    public var presenter: CountryViewPresentationLogic?
    public var worker: CountryWorkerProtocol?
    
    public init(aWorker: CountryWorkerProtocol) {
        worker = aWorker
    }
    
    func displayCountry() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.fetchCountry()
        }
    }
    
    func fetchCountry() {
        self.worker?.fetchCountry(completion: { (result) in
            switch result {
            case let .success(country):
                self.country = country
                self.presenter?.presentCountry(country: country)
            case let .failure(TaskError.Fetching(desc)):
                self.presenter?.presentError(errorDesc: desc)
            case let .failure(TaskError.Saving(desc)):
                self.presenter?.presentError(errorDesc: desc)
            }
        })
    }
    
    public func selected(countryAtIndexPath indexPath: IndexPath) {
        let selectedCountry = country[indexPath.row]
        var countryViewModel = CountryViewModel(name: selectedCountry.name, cioc: selectedCountry.cioc, population: "\(selectedCountry.population)", capital: selectedCountry.capital, alphaCode: selectedCountry.alphaCode)
        countryViewModel.image = selectedCountry.images?.first?.url()
        
        self.dataTransporter?.inbound(data: selectedCountry)
        self.dataTransporter?.inbound(viewModel: countryViewModel)
    }
    
}
