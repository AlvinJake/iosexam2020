//
//  CountryViewModel.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryViewModelProtocol {
    var name: String { get set }
    var cioc: String { get set }
    var population: String { get set }
    var capital: String { get set }
    var image: URL? { get set }
    var alphaCode: String { get set }
    
}

public struct CountryViewModel: CountryViewModelProtocol {
    public var name: String
    public var cioc: String
    public var population: String
    public var capital: String
    public var image: URL? = nil
    public var alphaCode: String
    
    init(name: String,cioc: String, population: String, capital: String, alphaCode: String) {
        self.name = name
        self.cioc = cioc
        self.population = population
        self.capital = capital
        self.alphaCode = alphaCode
    }
    
}

