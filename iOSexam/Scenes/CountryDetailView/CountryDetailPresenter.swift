//
//  CountryDetailPresenter.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryDetailDisplayLogic: class {
    func displayCountry(count: String)
    func display(country: CountryViewModelProtocol)
}

public protocol CountryDetailPresentationLogic {
    func presentCountry(country: CountryViewModelProtocol)
}

public class CountryDetailPresenter: NSObject, CountryDetailPresentationLogic {
    
    public weak var viewController: CountryDetailDisplayLogic?
    
    public func presentCountry(country: CountryViewModelProtocol) {
        viewController?.display(country: country)
    }
}

