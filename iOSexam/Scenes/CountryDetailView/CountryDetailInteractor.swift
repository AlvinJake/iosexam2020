//
//  CountryDetailInteractor.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol CountryDetailBusinessLogic {
    var appController: AppControllerProtocol! { get set }
    func loadSelectedCountry()
}

class CountryDetailInteractor: NSObject, CountryDetailBusinessLogic, DataTransportProtocol {
    
    public var appController: AppControllerProtocol!
    
    public var presenter: CountryDetailPresentationLogic?
    var receivedViewModel: CountryViewModelProtocol!
    var receivedData: CountryProtocol!
    
    public typealias ViewModel = CountryViewModelProtocol
    public typealias DataModel = CountryProtocol
    
    public func inbound(viewModel: CountryViewModelProtocol) {
        self.receivedViewModel = viewModel
    }
    
    public func inbound(data: CountryProtocol) {
        self.receivedData = data
    }
    
    public func outboundData() -> CountryProtocol? {
        return self.receivedData
    }
    
    public func outboundViewModel() -> CountryViewModelProtocol? {
        return self.receivedViewModel
    }
    
    public func loadSelectedCountry() {
        self.presenter?.presentCountry(country: self.receivedViewModel)
    }
    
}
