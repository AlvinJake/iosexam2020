//
//  AppInteractor.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Result

public protocol AppRoutingLogic {
    var controller:AppControllerProtocol! { get set }
    func moveToCountry()
}

public protocol AppControllerProtocol {
    var presenter: AppPresentationLogic!  { get set }
    var router: AppRoutingLogic! { get set }
    var interactor: AppBusinessLogic! { get set }
    
    func didLaunch()
    func restAPI()
}

public protocol AppRemoteCountryManagementProtocol {
    func fetchRemoteCountry(completion:  @escaping CountryRequestCompletionBlock)
}

public protocol AppCountryManagementProtocol {
    func fetchCountry(completion:  @escaping CountryFetchCompletionBlock)
}

public protocol AppBusinessLogic: AppCountryManagementProtocol, AppRemoteCountryManagementProtocol {
    
    var restAPI: APIProtocol! { get set }
    func initializeServices()
    func dataStore() -> DataStoreProtocol
}

public class AppInteractor: AppBusinessLogic {
    
    public var presenter: AppPresentationLogic!
    public var restAPI: APIProtocol!
    private var aDataStore: DataStoreProtocol!
    
    public init(dataStore: DataStoreProtocol) {
        self.aDataStore = dataStore
    }
    
    public func initializeServices() {
        restAPI.initialize()
    }
    
    public func dataStore() -> DataStoreProtocol {
        return aDataStore
    }
}

// MARK: User session tasks
extension AppInteractor {
    public func fetchCountry(completion: @escaping CountryFetchCompletionBlock) {
        fetchRemoteCountry { (result) in
            switch result {
            case .success(let data):
                self.dataStore().fetchCountry(completion: completion)
                print(data)
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}


extension AppInteractor {
    public func fetchRemoteCountry(completion:@escaping CountryRequestCompletionBlock) {
        restAPI.fetchRemoteCountry(completion: completion)
    }
}
