//
//  AppPresenter.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public protocol AppPresentationLogic
{
    
}

class AppPresenter: AppPresentationLogic {
    public var controller: AppControllerProtocol!
}
