//
//  CountryViewController.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import Kingfisher



class CountryViewController: BaseViewController,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CountryViewDisplayLogic {
    
    
    
    
    
    var interactor: CountryViewBusinessLogic!
    
    let screenSize = UIScreen.main.bounds
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    var array: NSMutableArray! = NSMutableArray()
    let tableData = [CountryViewModel]()
    var filteredTableData: [CountryViewModel] = []
    var resultSearchController = UISearchController()
    var searchActive : Bool = false

    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)
        
        
        
        let searchPredicate =  resultSearchController.searchBar.text
        print(searchPredicate!)
        
        
        searchActive = true
        
        let array = countryViewModels.filter({$0.name.lowercased().prefix(searchPredicate!.count) == searchPredicate!.lowercased()})
        filteredTableData = array
        
        
        
        self.collectionView.reloadData()
        
    }

    
    
  var searchBarViewConfig: SearchBarViewConfig!
    
    var countryViewModels: [CountryViewModel]!
    
//    //Place code in order of execution
    func confirgureSearchBarView() {
        self.searchBarViewConfig = SearchBarViewConfig(hostViewController: self, forController: resultSearchController);
        self.searchBarViewConfig.searchBarDelegate = self
        self.searchBarViewConfig.searchControllerDelegate = self
        self.searchBarViewConfig.searchResultsUpdatingDelegate = self
        self.searchBarViewConfig.configure()
        

        self.collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        

    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
           
            
            return controller
        })()
        
        // Reload the table
        
        collectionView.reloadData()
       
        confirgureSearchBarView()

        countryViewModels = [CountryViewModel]()
        interactor.displayCountry()

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
        self.collectionView.reloadData()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.collectionView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.collectionView.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.collectionView.reloadData()
    }



}
}
extension CountryViewController{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
                return 8
        
            }
    
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return CGSize(width: screenSize.width - 16, height: 120);
            }
    
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                if (searchActive) {
                    
                    return self.filteredTableData.count
                } else {
                    return self.countryViewModels.count
                }
        
            }
    
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCollectionViewCell", for: indexPath) as! CountryCollectionViewCell
                
                if (searchActive) {
                   
            
                    let cellModel = filteredTableData[indexPath.row]
                    cell.countryNameLabel.text = cellModel.name
                    cell.cioc.text = cellModel.cioc
                    cell.imageView.kf.setImage(with: cellModel.image, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                    return cell
                }
                else {
                    let cellModeltwo = countryViewModels[indexPath.row]
                    cell.countryNameLabel.text = cellModeltwo.name
                    cell.cioc.text = cellModeltwo.cioc
//                    print(countryViewModels[indexPath.row])
                    cell.imageView.kf.setImage(with: cellModeltwo.image, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    return cell
                }
            }
    
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                let newIndexPath = countryViewModels[indexPath.row]
                
            
                print(countryViewModels[indexPath.row])
                if resultSearchController.isActive {
                    
                interactor.selected(countryAtIndexPath: indexPath)
                performSegue(withIdentifier: "CountryDetail", sender: nil)
                }
            }
    func reloadCountry(country: [CountryViewModel]) {
        self.countryViewModels = country
           self.collectionView.reloadData()
    }
    
}
