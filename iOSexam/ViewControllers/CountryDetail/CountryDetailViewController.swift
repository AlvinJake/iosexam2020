//
//  CountryDetailViewController.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class CountryDetailViewController: BaseViewController, UITableViewDelegate, CountryDetailDisplayLogic {
    func displayCountry(count: String) {
        
    }
    var disposeBag = DisposeBag()
    
    var interactor: CountryDetailBusinessLogic? {
        didSet {
            
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel: CountryViewModelProtocol?
    var router: CountryDetailRoutingLogic?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.loadSelectedCountry()
    }
}

extension CountryDetailViewController {
    func display(country: CountryViewModelProtocol) {
        let models = [country]
        
        let items = Observable.just(models)
        
        items
            .bind(to: tableView.rx.items(cellIdentifier: "DetailViewCell", cellType: CountryDetailViewCell.self)) { (row, element, cell) in
                let model = element
                cell.countryNameLabel.text = (model.name.uppercased(with: nil))
                cell.countryPopulationLabel.text = model.population 
                cell.countryImageView.kf.setImage(with: model.image)
                cell.countryCapital.text = model.capital
                cell.countryAlphaCode.text = model.alphaCode
                
            }
            .disposed(by: disposeBag)
    }
}
