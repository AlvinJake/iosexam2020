//
//  CountryDetailViewCell.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit

class CountryDetailViewCell: UITableViewCell {

   
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryPopulationLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryCapital: UILabel!
    @IBOutlet weak var countryAlphaCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
