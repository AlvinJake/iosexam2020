//
//  Protocols.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Result


public typealias CountryRequestCompletionBlock = (Result<[Country], TaskError>) -> ()
public typealias CountryFetchCompletionBlock = (Result<[CountryProtocol], TaskError>) -> ()


public protocol UserProtocol {
    
}

public protocol ViewControllerProtocol {
    var appController: AppControllerProtocol! { get set }
}

public protocol APIFetchProtocol {
    func fetchRemoteCountry(completion: @escaping CountryRequestCompletionBlock)
}

public protocol APIProtocol: APIFetchProtocol {
    func initialize()
}

public protocol DataStoreProtocol {
    func fetchCountry(completion: @escaping CountryFetchCompletionBlock)
    
}
