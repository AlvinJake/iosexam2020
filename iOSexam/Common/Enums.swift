//
//  Enums.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

public enum TaskError: Error {
    case Saving(String)
    case Fetching(String)
}

