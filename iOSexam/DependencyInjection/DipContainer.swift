//
//  DipContainer.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Dip

extension UIViewController {
    enum Tags {
        static let countryView = "countryView"
        static let countryDetailView = "countryDetailView"
    }
}

extension AppDelegate {
    func setupDIP(container: DependencyContainer) {
        
        let dependencyFactory = DependencyFactory()
        
        container.register(.singleton) {
            CountryDetailInteractor() as CountryDetailInteractor
        }
        
        container.register(.singleton) { AppController() as AppControllerProtocol }
            .resolvingProperties { (container, controller) in
                var appController = controller
                let interactor = AppInteractor(dataStore: dependencyFactory.dataStore())
                let presenter = AppPresenter()
                let router = AppRouter()
                
                router.window = self.window!
                router.storyboard = self.mainStoryboard
                router.controller = controller
                appController.router = router
                appController.interactor = interactor
                presenter.controller = appController
                interactor.presenter = presenter
                interactor.restAPI = dependencyFactory.remoteAPI()
        }
        
        
        
        container.register(tag: UIViewController.Tags.countryView) { CountryViewController() }
            .resolvingProperties { (container, controller) in
                let appController = try! container.resolve() as AppControllerProtocol
                controller.appController = appController
                controller.title = NSLocalizedString("Country", comment: "country title");
                
                let countryWorker = CountryWorker()
                let viewController = controller
                let interactor = CountryInteractor(aWorker: countryWorker)
                let presenter = CountryPresenter()
                let router = CountryRouter()
                
                countryWorker.countryAppInterface = controller.appController.interactor
                viewController.interactor = interactor
                interactor.presenter = presenter
                interactor.worker = countryWorker
                interactor.appController = appController
                interactor.dataTransporter = AnyDataTransport(try! container.resolve() as CountryDetailInteractor)
                presenter.viewController = viewController
                router.viewController = viewController
        }
        
        container.register(tag: UIViewController.Tags.countryDetailView) { CountryDetailViewController() }
            .resolvingProperties { (container, controller) in
                let appController = try! container.resolve() as AppControllerProtocol
                let interactor = try! container.resolve() as CountryDetailInteractor
                let presenter = CountryDetailPresenter()
                let router = CountryDetailRouter()
                
                controller.appController = try! container.resolve() as AppControllerProtocol
                controller.interactor = interactor
                controller.router = router
                interactor.appController = appController
                interactor.presenter = presenter
                presenter.viewController = controller
                router.viewController = controller
        }
        DependencyContainer.uiContainers = [container]
    }
}
