//
//  DependencyFactory.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

protocol DependencyFactoryProtocol {
    func remoteAPI () -> APIProtocol
//    func FakeRemoteAPI () -> APIProtocol
    func dataStore () -> DataStoreProtocol
//    func FakedataStore () -> DataStoreProtocol
}

struct DependencyFactory: DependencyFactoryProtocol {
    var env = Configuration.sharedInstance.environment {
        didSet {
            
        }
    }
    
    func remoteAPI() -> APIProtocol {
        print("current env: \(env)")
        
            return RemoteAPI()
        
    }
    
    func dataStore() -> DataStoreProtocol {
        
            return DataStore()
    }
    
   
}
