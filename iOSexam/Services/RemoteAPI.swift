//
//  RemoteAPI.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit
import Result
import Alamofire
import CoreData
import SwiftyJSON

class RemoteAPI: NSObject, APIProtocol {
    
    var countryResults: [Country] = []
    var imageResult: [CountryImage] = []
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    
    func initialize() {
        
    }
    
    func fetchRemoteCountry(completion: @escaping CountryRequestCompletionBlock) {
//        Alamofire.request("https://restcountries.eu/rest/v2/all", method: .get).responseJSON { (responseData) -> Void in
//            if((responseData.result.value) != nil) {
//                let swiftyJsonVar = JSON(responseData.result.value!)
//
//
////                print(swiftyJsonVar)
//                if let resData = JSON(swiftyJsonVar)["region"].arrayObject {
//                    self.arrRes = resData as! [[String:AnyObject]]
//                    print(resData)
//                }
//                if self.arrRes.count > 0 {
////                    self.tblJSON.reloadData()
//                }
//            }
//        }
        DispatchQueue.main.async {
            completion(Result.success(self.countryResults))
            
        }
    }
}
            
