//
//  DataStore.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import Result
import Alamofire
import CoreData


class DataStore: NSObject, DataStoreProtocol {
    
    var countryResults: [Country] = []
    var imageResult: [CountryImage] = []
    
    
    func fetchCountry(completion: @escaping CountryFetchCompletionBlock) {
        
        Alamofire.request("https://restcountries.eu/rest/v2/all", method: .get, encoding: JSONEncoding.default).responseJSON { response in
            

            switch response.result {
            case .success( _):
                print("success")
                //to get JSON return value
                if response.result.value != nil {
                    print("not nil")
                    let result = response.result.value
                    
                    if let array = result as? [[String: Any]] {
                        let countryName = array.compactMap { $0["name"] as? String }
                        let cioc = array.compactMap { $0["cioc"] as? String }
                        let flag = array.compactMap { $0["flag"] as? String }
                        let capital = array.compactMap { $0["capital"] as? String }
                        let alphaCode = array.compactMap { $0["alpha3Code"] as? String }
                        let population = array.compactMap { $0["population"] as? Double }
                        
                        print(array.count)
                        
                        for i in 0..<min(countryName.count, cioc.count, capital.count, alphaCode.count, population.count) {
                            var country = Country(name: countryName[i], cioc: cioc[i], population: population[i], capital: capital[i], alphaCode: alphaCode[i])
                        
                           
                        let image =  CountryImage(stringUrl: flag[i])
                        country.images = [image]
                        self.countryResults.append(country)
                        }
                    }
                    //then use guard to get the value your want
                    
                }
                
            case .failure(let error):
                print(error)
            }
//            if let result = response.result.value {
//
//                let resultDictionary = result as! NSDictionary
//
//                if let countries = resultDictionary.object(forKey: "results") as! Array<NSDictionary>? {
//
//                    for country in countries {
//
//                        let countryName = country.value(forKey: "name") as? String
//                        let cioc = country.value(forKey: "cioc") as? String
//                        let flag = country.value(forKey: "flag") as? String
//                        let capital = country.value(forKey: "capital") as? String
//                        let alphaCode = country.value(forKey: "alpha3Code") as? String
//                        let population = country.value(forKey: "population") as? Double
//
//                        var country = Country(name: countryName ?? "no name", cioc: cioc ?? "no cioc", population: population ?? 0.0, capital: capital ?? "no name", alphaCode: alphaCode ?? "no code")
//                        let image =  CountryImage(stringUrl: flag ?? "no image")
//
//                        country.images = [image]
//                        self.countryResults.append(country)
//
//
//                    }
                    let country: [CountryProtocol] = self.countryResults
                    completion(Result.success(country))
                }
            }
        }
//
//    }
//
//}
