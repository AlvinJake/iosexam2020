//
//  Configuration.swift
//  iOSexam
//
//  Created by Alvin Jake Jebulan on 07/01/2020.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import UIKit

class Configuration {
    static let sharedInstance = Configuration()
    lazy var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            print("configuration: \(configuration)")
            let stringKey = "staging"
            
            if configuration.lowercased().range(of: stringKey) != nil {
                print("configuration: \(configuration)")
                return Environment.Staging
            }
        }
        
        return Environment.Production
    }()
}

enum Environment: String {
    case Staging = "staging"
    case Production = "production"
    
    var baseURL: String {
        switch self {
        case .Staging: return "https://staging-api.myservice.com"
        case .Production: return "https://api.myservice.com"
        }
    }
    
    var token: String {
        switch self {
        case .Staging: return "lktopir156dsq16sbi8"
        case .Production: return "5zdsegr16ipsbi1lktp"
        }
    }
}
